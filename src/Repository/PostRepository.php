<?php

namespace App\Repository;

use App\Entity\Post;


class PostRepository
{

    public function findAll(): array
    {

        $posts = [];

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM post");

        $query->execute();

        $results = $query->fetchAll();
        foreach ($results as $line) {
            $posts[] = $this->sqlToPost($line);
        }

        return $posts;
    }

    // public function carouselPost()
    // {
    //     # code...
    // }

    public function findLastRows(): array
    {
        $posts = [];

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM post order by id_post desc limit 6");

        $query->execute();

        $results = $query->fetchAll();
        foreach ($results as $line) {
            $posts[] = $this->sqlToPost($line);
        }
        return $posts;
    }

    public function add(Post $new_post): void
    {

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("INSERT INTO post (img, title, contain, date, autor) 
                                        VALUES (:img, :title, :contain, NOW(), :autor)");
        $query->bindValue(":img", $new_post->img, \PDO::PARAM_STR);
        $query->bindValue(":title", $new_post->title, \PDO::PARAM_STR);
        $query->bindValue(":contain", $new_post->contain, \PDO::PARAM_STR);
        $query->bindValue(":autor", $new_post->autor, \PDO::PARAM_STR);
        $query->execute();

        $new_post->id = $connection->lastInsertId();
    }

    public function remove(Post $new_post): void
    {

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("DELETE FROM post WHERE id_post = :id_post");
        $query->bindValue(':id_post', $new_post->id, \PDO::PARAM_INT);
        $query->execute();
    }

    public function update(Post $new_post)
    {

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("UPDATE post SET img = :img, title = :title, contain = :contain, autor = :autor WHERE id_post = :id_post");
        $query->bindValue(':id_post', $new_post->id, \PDO::PARAM_INT);
        $query->bindValue(":img", $new_post->img, \PDO::PARAM_STR);
        $query->bindValue(":title", $new_post->title, \PDO::PARAM_STR);
        $query->bindValue(":contain", $new_post->contain, \PDO::PARAM_STR);
        $query->bindValue(":autor", $new_post->autor, \PDO::PARAM_STR);

        $query->execute();
    }

    public function find(int $id): ? Post
    {

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM post WHERE id_post = :id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();

        if ($line = $query->fetch()) {
            return $this->sqlToPost($line);
        }
        return null;
    }

    private function sqlToPost(array $line): Post
    {

        $new_post = new Post();
        $new_post->id = intval($line["id_post"]);
        $new_post->img = $line["img"];
        $new_post->title = $line["title"];
        $new_post->contain = $line["contain"];
        $new_post->date = $line["date"];
        $new_post->autor = $line["autor"];
        return $new_post;
    }

    public function search(String $words)
    {
        $tabWords = [];

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare('SELECT * FROM post WHERE title LIKE :words');
        $query->bindValue(":words", '%' . $words . '%' , \PDO::PARAM_STR);
        $query->execute();

        $results = $query->fetchAll();
        foreach ($results as $line) {
            $tabWords[] = $this->sqlToPost($line);
        }
        return $tabWords;
    }
}
